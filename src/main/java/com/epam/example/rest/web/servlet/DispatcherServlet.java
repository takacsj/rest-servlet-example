package com.epam.example.rest.web.servlet;

import com.epam.example.rest.web.exception.NoMappingForHandlerException;
import com.epam.example.rest.web.handler.RESTHandler;
import com.epam.example.rest.web.handler.impl.UserHandler;
import com.epam.example.rest.web.response.ResponseEntity;
import com.epam.example.rest.web.response.factory.ResponseFactory;
import com.epam.example.rest.web.util.JSONTransformer;
import com.google.common.collect.ImmutableList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class DispatcherServlet extends HttpServlet {
    private static final List<RESTHandler> HANDLERS = ImmutableList.<RESTHandler>builder()
        .add(new UserHandler())
        .build();

    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String path = req.getPathInfo();
        String reqMethod = req.getMethod();
        try {
            RESTHandler handler = getHandler(path);
            ResponseEntity responseEntity = callHandler(handler, reqMethod, req, path);
            sendResponse(responseEntity, resp);
        } catch (NoMappingForHandlerException e) {
            sendResponse(ResponseFactory.createErrorResponse(HttpServletResponse.SC_NOT_FOUND, "No mapping for the specified path."), resp);
        } catch (NoSuchMethodException e) {
            sendResponse(ResponseFactory.createErrorResponse(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Method not allowed."), resp);
        } catch (IllegalAccessException | InvocationTargetException e) {
            sendResponse(ResponseFactory.createErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Something went wrong."), resp);
            e.printStackTrace();
        }
    }

    private RESTHandler getHandler(String path) throws NoMappingForHandlerException {
        return HANDLERS.stream()
            .filter(handler -> matchesMapping(handler, path))
            .findFirst()
            .orElseThrow(NoMappingForHandlerException::new);
    }

    private ResponseEntity callHandler(RESTHandler handler, String reqMethod, HttpServletRequest req, String path)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method handlerMethod = Arrays.stream(handler.getClass().getMethods())
            .filter(method -> method.getName().equals(reqMethod.toLowerCase()))
            .findFirst()
            .orElseThrow(NoSuchMethodException::new);
        return (ResponseEntity) handlerMethod.invoke(handler, req, stripPath(handler.getMapping(), path));
    }

    private void sendResponse(ResponseEntity responseEntity, HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setStatus(responseEntity.getStatus());
        httpServletResponse.setHeader("Content-type", "application/json");
        if (responseEntity.getPayload() != null) {
            httpServletResponse.getWriter().write(JSONTransformer.toJSON(responseEntity.getPayload()));
            httpServletResponse.getWriter().flush();
        }
    }

    private boolean matchesMapping(RESTHandler handler, String path) {
        return path.startsWith(handler.getMapping());
    }

    private String stripPath(String mapping, String path) {
        int startIndex = path.indexOf(mapping) + mapping.length() + 1;
        return startIndex > path.length()
            ? ""
            : path.substring(startIndex);
    }
}