package com.epam.example.rest.web.response.factory;

import com.epam.example.rest.web.response.ResponseEntity;
import com.epam.example.rest.web.response.error.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;

public class ResponseFactory {
    private ResponseFactory() {}

    public static <T> ResponseEntity<T> createSuccessfulResponse(T payload) {
        return new ResponseEntity<>(HttpServletResponse.SC_OK, payload);
    }

    public static ResponseEntity<ErrorMessage> createErrorResponse(int status, String errorMessage) {
        return new ResponseEntity<>(status, new ErrorMessage(errorMessage));
    }
}
