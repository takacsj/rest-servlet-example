package com.epam.example.rest.web.response;

public class ResponseEntity<T> {
    private final int status;
    private final T payload;

    public ResponseEntity(int status, T payload) {
        this.status = status;
        this.payload = payload;
    }

    public int getStatus() {
        return status;
    }

    public T getPayload() {
        return payload;
    }
}
