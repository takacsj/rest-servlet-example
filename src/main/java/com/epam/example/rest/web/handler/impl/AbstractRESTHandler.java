package com.epam.example.rest.web.handler.impl;

import com.epam.example.rest.web.domain.BaseEntity;
import com.epam.example.rest.web.handler.RESTHandler;
import com.epam.example.rest.web.response.ResponseEntity;
import com.epam.example.rest.web.response.factory.ResponseFactory;
import com.epam.example.rest.web.util.JSONTransformer;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.nio.charset.Charset;
import java.util.List;

public abstract class AbstractRESTHandler<T extends BaseEntity> implements RESTHandler {
    private Class<T> entityClass;

    public AbstractRESTHandler() {
        entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public ResponseEntity get(HttpServletRequest httpServletRequest, String path) {
        ResponseEntity responseEntity;
        if ("".equals(path)) {
            responseEntity = ResponseFactory.createSuccessfulResponse(getAllEntities());
        } else {
            try {
                responseEntity = getAllEntities().stream()
                        .filter(actual -> actual.getId().equals(Integer.parseInt(path)))
                        .findFirst()
                        .map(ResponseFactory::createSuccessfulResponse)
                        .orElse(null);
                if (responseEntity == null) {
                    responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Entity not found.");
                }
            } catch (NumberFormatException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Path is incorrect.");
            }
        }
        return responseEntity;
    }

    public ResponseEntity post(HttpServletRequest httpServletRequest, String path) {
        ResponseEntity responseEntity;
        if ("".equals(path)) {
            try {
                T newEntity = JSONTransformer.fromJSON(IOUtils.toString(httpServletRequest.getInputStream(), Charset.defaultCharset()), entityClass);
                if (! containsId(newEntity.getId())) {
                    doAdd(newEntity);
                    responseEntity = ResponseFactory.createSuccessfulResponse(null);
                } else {
                    responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_CONFLICT, "This entity is already added to the entities list.");
                }
            } catch (JsonMappingException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Entity JSON invalid.");
            } catch (IOException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error reading input.");
            }
        } else {
            responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Path is incorrect.");
        }
        return responseEntity;
    }

    public ResponseEntity put(HttpServletRequest httpServletRequest, String path) {
        ResponseEntity responseEntity;
        if (!"".equals(path)) {
            try {
                int idToUpdate = Integer.parseInt(path);
                T updatedEntity = JSONTransformer.fromJSON(IOUtils.toString(httpServletRequest.getInputStream(), Charset.defaultCharset()), entityClass);
                if (containsId(idToUpdate)) {
                    doDelete(idToUpdate);
                    doAdd(updatedEntity);
                    responseEntity = ResponseFactory.createSuccessfulResponse(null);
                } else {
                    responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Entity not found.");
                }
            } catch (NumberFormatException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Path is incorrect.");
            } catch (JsonMappingException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Entity JSON invalid.");
            } catch (IOException e) {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error reading input.");
            }
        } else {
            responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Path is incorrect.");
        }
        return responseEntity;
    }

    public ResponseEntity delete(HttpServletRequest httpServletRequest, String path) {
        ResponseEntity responseEntity;
        try {
            int idToDelete = Integer.parseInt(path);
            if (containsId(idToDelete)) {
                doDelete(idToDelete);
                responseEntity = ResponseFactory.createSuccessfulResponse(null);
            } else {
                responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Entity not found.");
            }
        } catch (NumberFormatException e) {
            responseEntity = ResponseFactory.createErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Path is incorrect.");
        }
        return responseEntity;
    }

    private boolean containsId(int id) {
        return getAllEntities().stream().anyMatch(entity -> entity.getId().equals(id));
    }

    protected abstract List<T> getAllEntities();
    protected abstract void doAdd(T newEntity);
    protected abstract void doDelete(int idToDelete);
}
