package com.epam.example.rest.web.domain;

public class BaseEntity {
    private final Integer id;

    public BaseEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
