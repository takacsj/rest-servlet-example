package com.epam.example.rest.web.handler.impl;

import com.epam.example.rest.web.domain.User;
import java.util.*;

public class UserHandler extends AbstractRESTHandler<User> {
    private static final String HANDLER_MAPPING = "/user";

    private List<User> users = new ArrayList<User>() {{
        add(new User(1, "Jodzsi"));
        add(new User(2, "BirkaKitti"));
    }};

    @Override
    public String getMapping() {
        return HANDLER_MAPPING;
    }

    @Override
    protected List<User> getAllEntities() {
        return users;
    }

    @Override
    protected void doAdd(User newUser) {
        users.add(newUser);
        users.sort(Comparator.comparingInt(User::getId));
    }

    @Override
    protected void doDelete(int idToDelete) {
        Iterator<User> userIterator = users.iterator();
        while (userIterator.hasNext()) {
            User actualUser = userIterator.next();
            if (actualUser.getId().equals(idToDelete)) {
                userIterator.remove();
                break;
            }
        }
    }
}
