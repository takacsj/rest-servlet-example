package com.epam.example.rest.web.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JSONTransformer {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String toJSON(Object target) throws JsonProcessingException {
        return objectMapper.writeValueAsString(target);
    }

    public static <T> T fromJSON(String source, Class<T> clazz) throws IOException {
        return objectMapper.readValue(source, clazz);
    }

    private JSONTransformer() {}
}
