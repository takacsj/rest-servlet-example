package com.epam.example.rest.web.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User extends BaseEntity {
    private final String name;

    @JsonCreator
    public User(@JsonProperty(value = "id", required = true) Integer id, @JsonProperty(value = "name", required = true) String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
